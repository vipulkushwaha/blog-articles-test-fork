# SPIT FE, 2023-24 Git Activity

This is an activity to learn about **git** and **git** platforms.

The contents of this repository are contributed by FE SPIT students.

## [Index of Articles](/index.md)

![SPIT FE](images/fe.webp)
Students of Batch F1, F2, F3 & F4

## Getting Started

1. Download Git from the following link:

[Git Official Website](https://git-scm.com/)

2. Watch the instructional video:

[Git Activity | Getting Started](https://youtu.be/raIf6cuUQv4)
